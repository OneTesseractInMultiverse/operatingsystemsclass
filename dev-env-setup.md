# Development Environment Setup (Debian 10)

 This guide assumes that you have a regular installation of Debian 10 (64 bits)
 using KDE or Gnome desktop environment (Other desktop implementations might
 work as well but have not been tested yet).
 
 
## Granting administrative privileges to your user account
 
To avoid having to log out of our normal user and log back in as the root account, 
we can set up what is known as superuser or root privileges for our normal account. 
This will allow our normal user to run commands with administrative privileges by 
putting the word sudo before the command. 

```bash
# su -
# usermod -aG sudo ${USER}
# exit
```

## Install C/C++ Compiler and Development Tools (build-essential)

In this course, most of the code we will be writting will be in C or C++ programming
languages. In order to compile our applications we are going to need a C/C++ Compiler 
plus some basic development tools. The following commands will install the required
tools. 

```bash
# sudo apt update
# sudo apt install build-essential
# sudo apt install ccache
# sudo apt install cmake
```

## Lets now create our first program to test our new C/C++ development environment

To test our development environment, we will first create a basic C program,
compile it and run it. 

Lets start by creating a new directory for our code:

```bash
# cd ~/
# mkdir code &&  cd code
# mkdir first_program && cd first_program
# vi test.c
```

Then type the following C code:

```C
#include<stdio.h>
int main()
{
   int a, b, c;
   printf("Enter two numbers to add, separated by a space: ");
   scanf("%d%d",&a,&b);
   c = a + b;
   printf("The sum of equals %d\n",c);
   return 0;
}
```

Now let save the file (:wq!), and run: 

```bash
# gcc test.c -o test
# ccache gcc test.c -o test
# ./test
```

You should get the following output:

```bash
# Enter two numbers to add, separated by space: 34 56
# The sum of equals 90

```

## Get an IDE

For this course we recomment signing up to the [JetBrains Student Program](https://www.jetbrains.com/student/)
to get a free license of __Clion__ IDE. 


