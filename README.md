# Operating Systems Class 

Prof. Pedro Guzman (pedro.guzman@ulead.ac.cr)

# Contents

- [Development Environment Setup](dev-env-setup.md)
- [Processes](processes.md)
