## Memory Layout of a Process

The memory of a process essentially contain four parts or segments.

- __Text Segment__ This segment contains the machine-language instructions of the
  program run by the process. This segment is always read-only to prevent programs
  to corrupt their own code due to bad pointers. Since many instances of the same 
  program can be running at the same time, this particular segment is sharable 
  between processes so that a single copy of the program  code can be mapped into
  the virtual address space of all processes. 

- __Initialized Data Segment__ This segment contains global and static variables
  that are explicitely initialized. The values of these variables are loaded from
  the executable file. 

- __Uninitialized Data Segment__ contains global and static variables that are not 
  explicitly initialized. Before starting the program, the system initializes all 
  memory in this segment to 0. For historical reasons, this is often called the 
  bss segment, a name derived from an old assembler mnemonic for “block started 
  by symbol.” The main reason for placing global and static variables that are 
  initialized into a separate segment from those that are uninitialized is that, 
  when a program is stored on disk, it is not necessary to allocate space for the 
  uninitialized data. Instead, the executable merely needs to record the location 
  and size required for the uninitialized data segment, and this space is allocated 
  by the program loader at run time.

- __Stack Segment__ The stack is a dynamically growing and shrinking segment 
  containing stack frames. One stack frame is allocated for each currently called 
  function. A frame stores the function’s local variables (so-called automatic 
  variables), arguments, and return value.

- __Heap Segment__ The heap is an area from which memory (for variables) can be 
  dynamically allocated at run time. The top end of the heap is called the program 
  break.

## Process Creation

### fork() SYSCALL
Allows one process, the parent, to create a new process, the child. This is done 
by making the new child process an (almost) exact duplicate of the parent: the 
child obtains copies of the parent’s stack, data, heap, and text segments. 
The term fork derives from the fact that we can envisage the parent process 
as dividing to yield two copies of itself.

### exit() SYSCALL
Library function terminates a process, making all resources (memory, open file 
descriptors, and so on) used by the process available for subsequent reallocation 
by the kernel. The status argument is an integer that determines the termination 
status for the process. Using the wait() system call, the parent can retrieve 
this status.

### wait() SYSCALL
This system call has two purposes. First, if a child of this process has not yet 
terminated by calling exit(), then wait() suspends execution of the process until 
one of its children has terminated. Second, the termination status of the child 
is returned in the status argument of wait().
